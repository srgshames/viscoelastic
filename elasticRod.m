function Etot=elasticRod(Z, thickness, Kbar, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices, display)

% global variable_tracking_array
% 
% persistent n_calls;%<-this will be incrementing each time the function is called
% if isempty(n_calls)
%     n_calls=0;
% end
% 
% n_calls=n_calls+1;
% 
% if mod(n_calls, 20) == 0
%     fprintf('call num %d\n', n_calls)
%     variable_tracking_array = [variable_tracking_array;Z];
% end
%     

kappa0 = Kbar;

[ x, y ] = ParseCoordinates(Z, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices);

ds = 1./(numel(x)-1);
dl0=l0./nEdges;

dx=diff(x);
dy=diff(y);
dl=sqrt(dx.^2+dy.^2);
nx=0.5*(-dy(1:end-1)./dl(1:end-1)-dy(2:end)./dl(2:end));
ny=0.5*(dx(1:end-1)./dl(1:end-1)+dx(2:end)./dl(2:end));
tx=dx./dl;
ty=dy./dl;
dl2 = (dl0(1:end-1) + dl0(2:end))/2;
dtx=diff(tx)./dl2;
dty=diff(ty)./dl2;
kappa=dtx.*nx+dty.*ny;


stretching=sum(((dl-dl0)./l0).^2); % make it a strain
bending=sum((kappa-kappa0).^2)*ds^2;
Etot=stretching+(thickness^2/3)*bending;

if nargin>11 && display == 1
    fprintf('stretching energy %e, bending energy %e \n', stretching, (thickness^2/3)*bending);
end


