figure;
pause(0.00001);
frame_h = get(handle(gcf),'JavaFrame');
set(frame_h,'Maximized',1);
trans_fram = 1;
 vidfile = VideoWriter('testmovie3.mp4','MPEG-4');
 open(vidfile);
for i=[1:10:22034]
    [ x, y ] = ParseCoordinates( variable_tracking_array(i,:), L, L0, [NaN NaN] );
    if mean(y)<0
        trans_frame = i;
        break
    end
end
for i=[1:100]
    [ x, y ] = ParseCoordinates( variable_tracking_array(10000,:), L, L0, [NaN NaN] );
    plot(x,y,'LineWidth',2+3*i/100);
    axis([0 1 -0.2 0.2])
    set(gca,'visible','off')
    set(gcf,'color','w');
    pause(0.001)
    drawnow
    writeVideo(vidfile, getframe(gcf));
end
for i=[10000:20:floor(trans_frame*1.2)]
    [ x, y ] = ParseCoordinates( variable_tracking_array(i,:), L, L0, [NaN NaN] );
    plot(x,y,'LineWidth',5)
    axis([0 1 -0.2 0.2])
    set(gca,'visible','off')
    set(gcf,'color','w');
    pause(0.001)
    drawnow
    writeVideo(vidfile, getframe(gcf));
end
close(vidfile)
% for i=[floor(trans_frame*0.9):10:floor(trans_frame*1.3)]
%     [ x, y ] = ParseCoordinates( variable_tracking_array(i,:), L, L0, [NaN NaN] );
%     plot(x,y)
%     axis([0 1 -0.2 0.2])
%     pause(0.001)
% end

