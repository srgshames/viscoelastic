function [ vertX, vertY ] = ParseCoordinates(Z, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices)

vertX = zeros(1, nMidVertices);
vertY = zeros(1, nMidVertices);

for iSlice=1:nSlices
    vertX(zStartIdxs(iSlice)+iSlice-1:zStartIdxs(iSlice+1)+iSlice-2) = Z(zStartIdxs(iSlice):zStartIdxs(iSlice+1) - 1);
    vertY(zStartIdxs(iSlice)+iSlice-1:zStartIdxs(iSlice+1)+iSlice-2) = Z(nFreeVertices + zStartIdxs(iSlice):nFreeVertices + zStartIdxs(iSlice+1) - 1);
    if iSlice < nSlices
        vertX(zStartIdxs(iSlice+1)+iSlice-1) = x0(iSlice+1);
        vertY(zStartIdxs(iSlice+1)+iSlice-1) = 0;
    end
end

firstAngle = BoundaryAngles(1);
lastAngle = BoundaryAngles(2);

if ~isnan(firstAngle)
    vertX = [l0(1)/nEdges(1) * cos(firstAngle), vertX];
    vertY = [l0(1)/nEdges(1) * sin(firstAngle), vertY];
end
if ~isnan(lastAngle)
    vertX = [vertX, x0(end) - l0(end)/nEdges(end) * cos(lastAngle)];
    vertY = [vertY, l0(end)/nEdges(end) * sin(lastAngle)];
end

vertX = [0, vertX, x0(end)];
vertY = [0, vertY, 0];

end

