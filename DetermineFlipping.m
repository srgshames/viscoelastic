function [ DidBandFlip ] = DetermineFlipping( Z0, thickness, Kbar, theta, BoundaryAngles, Slices, TestFunc, plotResult )

[Z, ~] = CalcRodElastica(Z0, thickness, Kbar, theta, BoundaryAngles, Slices, plotResult);
DidBandFlip = TestFunc( Z, BoundaryAngles, Slices );

end

