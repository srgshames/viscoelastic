function [Z,fval]=CalcRodElastica(Z0, thickness, Kbar, theta, BoundaryAngles, Slices, plotResult)

nSlices = numel(Slices);
nFreeVertices = length(Z0)/2;
nMidVertices = nFreeVertices + nSlices - 1;

firstAngle = BoundaryAngles(1);
lastAngle = BoundaryAngles(2);

nNonTrivialVertices = nFreeVertices + ~isnan(firstAngle) + ~isnan(lastAngle);
zStartIdxs = [1, ceil(cumsum([Slices.nFraction]) * (nNonTrivialVertices+1)) - ~isnan(firstAngle)];
zStartIdxs(end) = zStartIdxs(end) - ~isnan(lastAngle);

nEdges = diff([1, ceil(cumsum([Slices.nFraction]) * (nNonTrivialVertices+1))]) + 1;
l0 = theta.*[Slices.xLengthNatural]./(2*sin(theta/2));
l0 = repelem(l0, nEdges);

nEdges = repelem(nEdges, nEdges);

x0 = [0, cumsum([Slices.xLengthActual])];

% close all;

opts = optimoptions(@fminunc,'Display','off','Algorithm','quasi-newton','MaxIter', 1E8,'MaxFunEvals', 1E8,'TolX', 1e-20,'TolFun', 1e-20);

fun=@(x)elasticRod(x, thickness, Kbar, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices);

[Z, fval]=fminunc(fun,Z0,opts);

if nargin>6 && plotResult == 1
    fprintf('initial: ')
    elasticRod(Z0, thickness, Kbar, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices, 1);
    fprintf('final: ')
    elasticRod(Z, thickness, Kbar, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices, 1);
    PlotZ(Z, Z0, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices);
end

end



