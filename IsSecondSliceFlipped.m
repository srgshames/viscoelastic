function [ DidFlip ] = IsSecondSliceFlipped( Z, BoundaryAngles, Slices )

n = length(Z)/2;

firstAngle = BoundaryAngles(1);
lastAngle = BoundaryAngles(2);

nMidVertices = n + ~isnan(firstAngle) + ~isnan(lastAngle) + 1;
zStartIdxs = [1, ceil(cumsum([Slices.nFraction]) * nMidVertices) - ~isnan(firstAngle)];

y = Z(n + zStartIdxs(2):n + zStartIdxs(3) - 1);
DidFlip = mean(y) > 0;

end

