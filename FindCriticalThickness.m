function [ tMin, tMax ] = FindCriticalThickness( Z0, Kbar, theta, BoundaryAngles, Slices, tMin, tMax, tolerance, TestFunc, plotResult )

if nargin<10
    plotResult=NaN;
end

if (tMax - tMin ) > tolerance
    thickness = sqrt(tMin*tMax);
    fprintf('tMin %e, tMax %e, thickness: %e \n',tMin, tMax, thickness);
    DidBandFlip = DetermineFlipping( Z0, thickness, Kbar, theta, BoundaryAngles, Slices, TestFunc, plotResult );
    
    if DidBandFlip
        [tMin, tMax] = FindCriticalThickness( Z0, Kbar, theta, BoundaryAngles, Slices, tMin, thickness, tolerance, TestFunc, plotResult );
    else
        [tMin, tMax] = FindCriticalThickness( Z0, Kbar, theta, BoundaryAngles, Slices, thickness, tMax, tolerance, TestFunc, plotResult );
    end

end

end

