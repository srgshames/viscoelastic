function [Z0, K0] = CreateInitialConditions( thetaNatural, n, BoundaryAngles, Slices, K0Factor, SlicesInitState )

firstAngle = BoundaryAngles(1);
lastAngle = BoundaryAngles(2);

verticesX = {};
verticesY = {};
K0 = {};

lNatural = [Slices.xLengthNatural];
L0 = thetaNatural.*lNatural./(2*sin(thetaNatural/2));

lActual = [Slices.xLengthActual];
x0 = [0, cumsum(lActual)];

nNonTrivialVertices = n + ~isnan(firstAngle) + ~isnan(lastAngle);
nMidVerticesArr = diff([1, ceil(cumsum([Slices.nFraction]) * (nNonTrivialVertices+1))]);

for iSlice=1:numel(Slices)
    K0{iSlice} = 2/lNatural(iSlice).*sin(thetaNatural/2) * ones(1, nMidVerticesArr(iSlice)+1) * K0Factor(iSlice);
    thetaActual = fzero(@(x) sin(x/2)-(x/2)*(lActual(iSlice)/L0(iSlice)), thetaNatural);
    rActual = lActual(iSlice)/(2*sin(thetaActual/2));
    [x, y] = Arc(thetaActual, rActual, nMidVerticesArr(iSlice) + 2);
    verticesX{iSlice} = x(2:end-1) + x0(iSlice);
    verticesY{iSlice} = y(2:end-1) * SlicesInitState(iSlice);
end

verticesX = cell2mat(verticesX);
verticesY = cell2mat(verticesY);

if ~isnan(firstAngle)
    verticesX = verticesX(2:end);
    verticesY = verticesY(2:end);
end
if ~isnan(lastAngle)
    verticesX = verticesX(1:end-1);
    verticesY = verticesY(1:end-1);
end

Z0 = [verticesX, verticesY];

K0 = cell2mat(K0);
K0 = 0.5*(K0(1:end-1)+K0(2:end));

end

