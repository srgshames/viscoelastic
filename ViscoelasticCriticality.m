
clear;
close all;

n = 61; % Magic number
L = 1;

theta = deg2rad(90);

Slices = struct('nFraction', num2cell(ones(1,3)*1/3), ...
                'xLengthNatural', num2cell(ones(1,3)*1/3*L), ...
                'xLengthActual', num2cell(ones(1,3)*1/3*L*0.99));

BoundaryAngles = [NaN NaN] ;
SlicesInitState = [-1, -1, 1];
K0Factor = [1, -1, 1];
[Z0, K0] = CreateInitialConditions( theta, n, BoundaryAngles, Slices, K0Factor, SlicesInitState );


%% Find critical thickness
% 
tMin = 1e-6/L; % We need to normalize the min,max (because t itself changes between different L's)
tMax = 1e+0/L;
figure('Name', 'Binary Search');
[tMin, tMax] = FindCriticalThickness( Z0, K0, theta, BoundaryAngles, Slices, tMin, tMax, tMin, @IsSecondSliceFlipped, 1);
fprintf('Finished with (Normalized) thickness %e \n', tMax/L); % Normalized variables


%% Compare different regimes around critical thickness

% tArr = linspace(2*tMin - tMax, 2*tMax - tMin, 20);
tArr = tMax + 3e+3*(tMax-tMin)*(-3:1:3);
% tArr = linspace(0.9*tMax, 1.1*tMax, 20);

regime = zeros(3, numel(tArr));
figure('Name', 'critical behavior');
hold on;

ReverseFirstAngle = [-theta/2 NaN];
SlicesReverseInitState = [1, -1, 1];
[Z0Reverse, K0Reverse] = CreateInitialConditions( theta, n, ReverseFirstAngle, Slices, K0Factor, SlicesReverseInitState );

RelaxedK0Factor = [1, -0.95, 0.95];
[Z0Relaxed, K0Relaxed] = CreateInitialConditions( theta, n, BoundaryAngles, Slices, RelaxedK0Factor, SlicesInitState );

for tIdx=1:numel(tArr)
    fprintf('tIdx %d\n',tIdx);    
    
    subplot(numel(tArr), 3, 3*tIdx-2);
    title([num2str(tArr(tIdx)), ', Basic']);
    regime(1, tIdx) = DetermineFlipping( Z0, tArr(tIdx), K0, theta, BoundaryAngles, Slices, @IsSecondSliceFlipped, 1 );
    
    subplot(numel(tArr), 3, 3*tIdx-1);
    title([num2str(tArr(tIdx)), ', Reverse']);
    regime(2, tIdx) = DetermineFlipping( Z0Reverse, tArr(tIdx), K0Reverse, theta, ReverseFirstAngle, Slices, @IsSecondSliceFlipped, 1 );
    
    subplot(numel(tArr), 3, 3*tIdx);
    title([num2str(tArr(tIdx)), ', Relaxed']);
    regime(3, tIdx) = DetermineFlipping( Z0Relaxed, tArr(tIdx), K0Relaxed, theta, BoundaryAngles, Slices, @IsSecondSliceFlipped, 1 );
end

figure('Name', 'critical phase space');
hold on;
plot(tArr, regime(1,:));
plot(tArr, regime(2,:));
plot(tArr, regime(3,:));