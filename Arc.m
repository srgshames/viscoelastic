function [ x, y ] = Arc(alpha,r,nSteps)
%  
%     alpha = pi/3;
%     r = 1;
%     nSteps = 52;

    x = linspace(r*cos((pi+alpha)/2), r*cos((pi-alpha)/2), nSteps);
    y = sqrt(r^2 - x.^2);
    x = x - x(1);
    y = y - y(1);
%     plot(x, y0);

end

