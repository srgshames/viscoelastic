function PlotZ(Z, Z0, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices )

[ x, y ] = ParseCoordinates(Z, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices );
[ xInitial, yInitial ] = ParseCoordinates( Z0, BoundaryAngles, x0, l0, zStartIdxs, nSlices, nEdges, nFreeVertices, nMidVertices );
% figure;
hold on;
plot(xInitial, yInitial, 'b'); 
plot(x, y, 'r');
% axis image