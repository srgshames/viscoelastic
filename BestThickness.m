
clear;
close all;

n = 61; % Magic number

L = 1;

thetaArr = [60:5:90]; % arc angle
thetaArr = deg2rad(thetaArr);

KbarTemplateArr = [1:-0.02:0.9];
thicknessMat = zeros(numel(thetaArr), numel(KbarTemplateArr));
Kbarlst = [];
thetaLst = [];
tLst = [];

Slices = struct('nFraction', num2cell(ones(1,3)*1/3), ...
                'xLengthNatural', num2cell(ones(1,3)*1/3*L), ...
                'xLengthActual', num2cell(ones(1,3)*1/3*L*0.99));
            
BoundaryAngles = [NaN NaN] ;
SlicesInitState = [-1, -1, 1];
K0Factor = [1, -1, 1];

for thetaIdx=1:numel(thetaArr)
    
    theta = thetaArr(thetaIdx);
    
    for KbarIdx=1:numel(KbarTemplateArr)
        Kbar = K0Factor*KbarTemplateArr(KbarIdx);
        
        [Z0, K0] = CreateInitialConditions( theta, n, BoundaryAngles, Slices, Kbar, SlicesInitState );

        tMin = 1e-5/L; % We need to normalize the min,max (because t itself changes between different L's)
        tMax = 1e-2/L;
%         figure('Name', 'Binary Search');
        [~, thickness] = FindCriticalThickness( Z0, K0, theta, BoundaryAngles, Slices, tMin, tMax, 1e-5/L, @IsSecondSliceFlipped);
        thicknessMat(thetaIdx, KbarIdx) = thickness;
        tLst = [tLst thickness];
        fprintf('Finished with (Normalized) Kbar %f, theta %f theta, thickness %e \n',KbarTemplateArr(KbarIdx)*L, theta, thickness/L); % Normalized variables
    end   
end

Kbarlst = repmat(KbarTemplateArr,1,numel(thetaArr));
thetaLst = repelem(thetaArr,numel(KbarTemplateArr));

% Plot results
figure
tri = delaunay(thetaLst,Kbarlst);
h = trisurf(tri, thetaLst, Kbarlst, tLst);
axis vis3d
lighting phong
shading interp
colorbar EastOutside
hold on
plot3(thetaLst,Kbarlst,tLst(1:end),'*') % fix the 6
xlabel('theta')
ylabel('Kbar')
zlabel('thickness')